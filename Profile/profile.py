# -*- coding: utf-8 -*-
import asyncio
import logging
import re
from collections import namedtuple
from typing import Optional, Union

import discord
from redbot.core import checks, Config, commands, bot

log = logging.getLogger("red.cbd-cogs.profile")

__all__ = ["UNIQUE_ID", "Profile"]

UNIQUE_ID = 0x62696F68617A61726422


class Profile(commands.Cog):
    """Add information to your player profile and lookup information others have shared.
    
    See `[p]help p` for detailed usage informaiton."""
    def __init__(self, bot: bot.Red, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.bot = bot
        self.conf = Config.get_conf(self, identifier=UNIQUE_ID, force_registration=True)
        self.conf.register_user(profile={})
        self.conf.register_guild(pgames=[])

    @commands.group(autohelp=False)
    @commands.guild_only()
    async def pgames(self, ctx: commands.Context):
        """List the available game fields
        
        Users will only be able to set a field in their profile if it has been added to this list"""
        if ctx.invoked_subcommand is not None:
            return
        pgames = await self.conf.guild(ctx.guild).pgames()
        if len(pgames):
            await ctx.send("Game fields available:\n" + ("\n".join(pgames)))
        else:
            await ctx.send("No game fields available. Alert an admin!")

    @pgames.command(name="add")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def add_field(self, ctx: commands.Context, *, argField: str):
        """Add fields to the list available for adding to profiles"""
        argField = argField.strip("\" '")
        pgames = await self.conf.guild(ctx.guild).pgames()
        for field in pgames:
            if field.lower() == argField.lower():
                await ctx.send(f"Field '{field}' already exists!")
                return
        pgames.append(argField)
        await self.conf.guild(ctx.guild).pgames.set(pgames)
        await ctx.send(f"Field '{argField}' has been added")

    @pgames.command(name="remove")
    @commands.guild_only()
    @checks.admin_or_permissions(manage_guild=True)
    async def remove_field(self, ctx:commands.Context, *args):
        """Remove fields from profiles and make them unavailable (DANGER!)
        
        USE WITH CAUTION: There is no way to restore deleted fields!"""
        pgames = await self.conf.guild(ctx.guild).pgames()
        argField = " ".join(args)
        try:
            pgames.remove(argField)
        except ValueError:
            for field in pgames:
                if field.lower() == argField.lower():
                    pgames.remove(field)
                    break
            else:
                await ctx.send(f"No field named '{argField}'")
                return

        await self.conf.guild(ctx.guild).pgames.set(pgames)
        count = 0
        for member, conf in (await self.conf.all_users()).items():
            memberProfile = conf.get("profile")
            if argField in memberProfile.keys():
                del memberProfile[argField]
                await self.conf.user(self.bot.get_user(member)).profile.set(memberProfile)
                count += 1
        await ctx.send(f"Removed field '{argField}' from {count} profiles")

    @commands.command()
    @commands.guild_only()
    async def p(self, ctx: commands.Context, userOrField: Optional[str] = None, *fields):
        """Display and modify your profile or view someone else's profile
        
        Examples:
        Display your own profile
        `[p]p`
        
        Display your friend's profile
        `[p]p @friend`
        
        Display the 'foo' and 'bar' fields on your friend's profile
        `[p]p @friend foo bar`
        
        Note that fields with spaces in the name must be in quotes
        `[p]p @friend 'Three Word Field'`
        
        Set the 'foo' field on your profile to 'bar'
        `[p]p foo bar`
        
        Remove the 'foo' field from your profile
        `[p]p foo`
        
        Other commands to look into:
        `[p]help pgames`
        """
        await self._p(ctx, userOrField, *fields)

    async def _p(self, ctx: commands.Context, user: Optional[str] = None, *args):
        pgames = await self.conf.guild(ctx.guild).pgames()
        key = None
        if re.search(r'<@!\d+>', str(user)):
            user = ctx.guild.get_member(int(user[3:-1]))
            if not user:
                await ctx.send("Unknown user")
                return
        if not isinstance(user, discord.abc.User):
            # Argument is a key to set, not a user
            key = user
            user = ctx.author
        pDict = await self.conf.user(user).profile()

        # User is setting own profile
        warnings = []
        if key is not None and user is ctx.author:
            if key not in pgames:
                keySwap = False
                for field in pgames:
                    if key.lower() == field.lower():
                        key = field
                        break
                else:
                    await ctx.send("Sorry, that pgame field is not available.\n"
                                   "Please request it from the server owner.")
                    return
            if args:
                pDict[key] = " ".join(args)
                await self.conf.user(user).profile.set(pDict)
                await ctx.send(f"Field '{key}' set to {pDict[key]}")
            else:
                try:
                    del pDict[key]
                except KeyError:
                    await ctx.send(f"Field '{key}' not found in your profile")
                    return
                await self.conf.user(user).profile.set(pDict)
                await ctx.send(f"Field '{key}' removed from your profile")
            return

        # Filter dict to key(s)
        elif user and len(args):
            data = {}
            for arg in args:
                try:
                    data[arg] = pDict[arg]
                except KeyError:
                    for field in pgames:
                        if arg.lower() == field.lower() and field in pDict.keys():
                            data[field] = pDict[field]
                            break
                    else:
                        warnings.append(f"Field '{arg}' not found")
            pDict = data
        embed = discord.Embed()
        embed.title = f"{user.display_name}'s Profile"
        embed.set_thumbnail(url=user.avatar_url)
        embed.set_footer(text="\n".join(warnings))
        for field, value in pDict.items():
            embed.add_field(name=field, value=value, inline=False)
        await ctx.send(embed=embed)

    @pgames.command(name="search")
    @commands.guild_only()
    async def pgame_search(self, ctx: commands.Context, *args):
        """Find field values across all users
        
        Examples:
        Search for a single field 'foo'
        `[p]pgames search foo`
        
        Search for multiple fields 'foo', 'bar', and 'long name field'
        `[p]pgames search foo bar 'long name field'`
        """
        argsLower = [x.lower() for x in args]
        embed = discord.Embed()
        embed.title = "Profile Search"
        for member, conf in (await self.conf.all_users()).items():
            memberProfile = conf.get("profile")
            if len(args) > 1:
                values = [f"{x}: {y}" for x,y in memberProfile.items() if x.lower() in argsLower]
            else:
                values = [y for x,y in memberProfile.items() if x.lower() in argsLower]
            if len(values):
                try:
                    memberName = ctx.guild.get_member(int(member)).display_name
                except:
                    continue
                embed.add_field(name=memberName,
                                value="\n".join(values),
                                inline=False)
        await ctx.send(embed=embed)

    @commands.command()
    @commands.guild_only()
    async def psearch(self, ctx: commands.Context, *args):
        """Find a user's Discord display name via a field and value
        
        Examples:
        Search for a single user's display name based on field 'foofield' and value 'foousername'
        `[p]psearch foofield foousername`
        
        Search for multiple user's display names
        `[p]psearch foofield foousername "bar long username"
        """
        argsLower = [x.lower() for x in args]

        if(len(argsLower) > 0):
            field = argsLower[0]

        if(len(argsLower) > 1):
            usernames = argsLower[1:]
        embed = discord.Embed()
        embed.title = f"Discord user Search for {field}"
        for member, conf in (await self.conf.all_users()).items():
            memberProfile = conf.get("profile")
            if len(args) > 1:
                username = [y for x,y in memberProfile.items() if x.lower() == field and any(re.match(f"{line}.*", y.lower()) for line in usernames) ]
            else:
                embed.add_field(name="Error", value="You must include a field and a value")
                break
            if len(username):
                try:
                    memberName = ctx.guild.get_member(int(member)).display_name
                except:
                    continue
                embed.add_field(name="Username: ".join(username),
                                value=f"\nDisplay Name: {memberName}",
                                inline=False)
        await ctx.send(embed=embed)
